# AngularInterview

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 15.2.4.

```sh
# Summary
This project has a component called `AutoCompleteComponent`.
 - The component renders a text input field with auto-completion. It fetchs the items using `CountriesServices` that returns an `Observable<string[]>``
 - The component renders a div element that has the class name wrapper and two child elements: a div element with the class name control and a div element with the class name list. The div element with class name control contains an input element with the class name form-control, and this is the input in which the user enters a query.
 - Once a response comes from the API, all strings from the response are be displayed inside the div tag with class name list, each one inside a separate a element with the class name list-item. The strings are displayed in the same order as they arrived in from the API.
 - The component avoid sending too many requests to the API; in particular, it do not send requests on every single keypress. It debounce the requests. The debounce time-out is 500 milliseconds.
 - When items are being fetched, a class name is-loading is added to the input's wrapper (the element with class name control).
 - When items are being fetched, no request has been sent, or the endpoint has returned zero items, the div element with class name list does not be rendered.
 - The component emits string typed event with an output property onSelect. Event is emitted whenever user click on an item from autocomplete list, and contain name of selected item. Clicking on an item does not have any effect apart from emitting event.

This project has a service called `CountriesServices` responsible of get the Countries from a public API:
- It do a GET request to the API https://restcountries.com/v3.1/ sending the user input in this way https://restcountries.com/v3.1/name/:userInput
- If the API returns a 404 the service returns an empty array instead of throw the error.
```

```sh

```

```sh
# Task
You should complete the tests under `autocomplete.component.spec.ts` and `countries.service.spec.ts`
```
