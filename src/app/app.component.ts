import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
},)
export class AppComponent {
  onSelect(country: string) {
    alert(`Select: ${country}`);
  }
}
