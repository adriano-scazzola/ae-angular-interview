import { ComponentFixture, TestBed } from '@angular/core/testing';
import { CountriesService } from "src/services/countries.service";
import { AutocompleteComponent } from "./autocomplete.component";
import { of } from 'rxjs';


describe('AutocompleteComponent', () => {
  let component: AutocompleteComponent;
  let fixture: ComponentFixture<AutocompleteComponent>;

  beforeEach(async () => {
    const countriesService = jasmine.createSpyObj<CountriesService>('CountriesService', {
      getCountries: of([])
    });

    await TestBed.configureTestingModule({
      declarations: [AutocompleteComponent],
      providers: [{ provide: CountriesService, useValue: countriesService }]
    })
      .compileComponents();

    fixture = TestBed.createComponent(AutocompleteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('should send request and debouncing exactly one request is sent 500ms after a query is typed in', () => {
  });
  it('Solution sending requests and denouncing no request is sent 200ms after a query is typed in, but after next 300ms exactly one request is send', () => {
  });
  it('Solution sending requests and denouncing after: typing "q" in, 100ms passes by, appending "u", 450ms passes by no request is sent, but it is sending exactly one request after another 50ms', () => {
  });
  it('Solution sending requests and denouncing two requests are sent after: typing "q" in, 500ms passes by, typing "u" in, 550ms passes by', () => {
  });
  it('Solution `is-loading` class `is-loading` class is not added right after typing in a query, but it is after sending a request', () => {
  });
  it('Solution `is-loading` class `is-loading` class is not present when response has come', () => {
  });
  it('Solution `is-loading` class `is-loading` class is not present after: typing "q" in, 100ms passes by, typing "u" in, 450ms passes by; but it is after next 50 ms', () => {
  });
  it('Solution list content 300ms after a query is typed in, the list section is not present', () => {
  });
  it('Solution list content after a request is sent, the list section is still absent', () => {
  });
  it('Solution list content after a response comes in, the list section is already present', () => {
  });
  it('Solution list content the list section is absent when the response consists of zero items', () => {
  });
  it('Solution onSelect output property onSelect is called when the first item is clicked', () => {
  });
  it('Solution onSelect output property onSelectItem is called twice when the two items are clicked', () => {
  });
});
