import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Subject, Observable, debounceTime, filter, tap, switchMap, map, merge } from 'rxjs';
import { CountriesService } from '../../services/countries.service';

@Component({
  selector: 'autocomplete-input',
  templateUrl: './autocomplete.component.html',
  styleUrls: ['./autocomplete.component.sass']
})
export class AutocompleteComponent implements OnInit {
  private inputValueChanged$ = new Subject<string>();

  @Output() onSelect = new EventEmitter<string>();
  loading = false;
  items$!: Observable<string[]>;

  constructor(private countriesService: CountriesService) { }

  ngOnInit(): void {
    const input$ = this.inputValueChanged$.pipe(
      debounceTime(500));

    const inputWithValue$ = input$.pipe(filter(x => !!x));

    const inputWithoutValue$ = input$.pipe(filter(x => !x));

    const inputWithValueSearch$ = inputWithValue$.pipe(
      tap(() => this.loading = true),
      switchMap(x => this.countriesService.getCountries(x)),
      map(x => x.map(y => y.name.official)),
      tap(() => this.loading = false)
    );

    const inputWithoutValueSearch$ = inputWithoutValue$.pipe(
      map(() => [])
    );


    this.items$ = merge(inputWithValueSearch$, inputWithoutValueSearch$);
  }

  inputChanged(event: Event) {
    const element = event.currentTarget as HTMLInputElement
    this.inputValueChanged$.next(element.value);
  }

  onPaste(event: ClipboardEvent) {
    const pastedText = event.clipboardData?.getData('text');
    this.inputValueChanged$.next(pastedText || '');
  }

  selectItem(item: string) {
    this.onSelect.emit(item);
  }
}
