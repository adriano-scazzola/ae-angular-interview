export interface CountryName {
    official: string;
    common: string;
}