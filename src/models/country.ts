import { CountryName } from "./country-name";

export interface Country {
    name: CountryName;
    independent: boolean;
}
