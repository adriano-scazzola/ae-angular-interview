import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, catchError, of } from 'rxjs';
import { Country } from '../models/country';

const API_URL = "https://restcountries.com/v3.1/name";

@Injectable({ providedIn: 'root' })
export class CountriesService {
    constructor(private http: HttpClient) { }

    getCountries(query: string): Observable<Country[]> {
        return this.http.get<Country[]>(`${API_URL}/${query}`).pipe(
            catchError(x => {
                if (x.status === 404) {
                    return of([]);
                }
                console.error('error');
                throw x;
            })
        );
    }
}